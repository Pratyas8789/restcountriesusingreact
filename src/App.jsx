import React, { useState } from 'react'
import './App.css'
import NavBar from './Components/NavBar/NavBar'
import CountriesContainer from './Components/CountriesContainer/CountriesContainer'
import Service from './Components/Service/Service'
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom'
import CountriesDetails from './Components/CountriesDetails/CountriesDetails'

function App() {
  const [theme, setTheme] = useState(true)
  const [data, setData] = useState("")

  const handleTheme = () => {
    setTheme(!theme)
  }

  const handleData = (data) => {
    setData(data)
  }

  return (
    <div className="App" >
      <Router>
        <Routes>
          <Route exact path='/' element={
            <>
              <NavBar mode={theme} changeTheme={handleTheme} />
              <CountriesContainer data={data} mode={theme} />
              <Service handleData={handleData} />
            </>
          } />
          <Route exact path='/:capitalName' element={<CountriesDetails mode={theme} changeTheme={handleTheme} />} />
        </Routes>
      </Router>

    </div>
  )
}
export default App