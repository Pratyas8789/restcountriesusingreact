import React, { useEffect } from 'react'

export default function Service(props) {
  const { handleData } = props
  let data;

  const getData = (data1) => {
    handleData(data1)
  }

  useEffect(() => {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => {
        data = response.json()
        return data
      })
      .then((data1) => {
        getData(data1)
      })
  }, [])

  return (
    <div>
    </div>
  )
}
