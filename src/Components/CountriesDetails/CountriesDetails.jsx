import React, { useEffect, useState } from 'react'
import './CountriesDetails.css'
import { Link, useParams } from "react-router-dom"
import FindCountryLanguage from '../FindCountryLanguage/FindCountryLanguage'

export default function CountriesDetails(props) {
    const { mode, changeTheme } = props
    const [countryData, setCountryData] = useState("")
    const { capitalName } = useParams()
    useEffect(() => {
        fetch(`https://restcountries.com/v3.1/capital/${capitalName}`)
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                setCountryData(data)
            })
    }, [])


    return (

        <>
            {countryData !== "" ?
                <div className={`CountriesDetails-container ${mode ? "dark" : "light"}`} >
                    <div className="navBar">
                        <h2>Where in the world? </h2>
                        <h3 onClick={changeTheme} className="changeTheme" > <i className="fa fa-moon-o" aria-hidden="true"></i>Dark Mode</h3>
                    </div>
                    <div className="mainContainer">
                        <div className="backButton">
                            <Link to="/" >
                                <button>
                                    <i className="fa fa-long-arrow-left" aria-hidden="true"></i>Back
                                </button>
                            </Link>
                        </div>
                        <div className="aboutCountry">
                            <div className="flagg">
                                <img src={countryData[0].flags.png} alt="" />
                            </div>
                            <div className="allInformation">
                                <div className="info">
                                    <div className="leftInfo">
                                        <h2>{countryData[0].name.common}</h2>
                                        <p><h4>Native Name : </h4>{countryData[0].name.nativeName?.[Object.keys(countryData[0].name.nativeName)[0]].common || 'N/A'}</p>
                                        <h4>Population :{countryData[0].population}</h4>
                                        <h4>Region:{countryData[0].region}</h4>
                                        <h4>Sub Region:{countryData[0].subregion}</h4>
                                        <h4>Capital:{countryData[0].capital}</h4>

                                    </div>
                                    <div className="rightInfo">
                                        <h4>Total Level domain: be</h4>
                                        <h4>Currencies: {Object.values(countryData[0].currencies)?.[0]?.name || "NA"}</h4>
                                        <h4>Languages:<FindCountryLanguage languageData={countryData[0].languages} /></h4>
                                    </div>
                                </div>

                                <div className="borderCountries">
                                    <h4>Border Countries: </h4>
                                    {countryData[0].borders?.length ? countryData[0].borders.map(border => <div className='borderCountries-div' key={border}>{border}</div>) : <div className="borderCountries-div">NA</div>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                : ""}
        </>

    )
}
