import React from 'react'

export default function OptionCard(props) {
    const { defaultName, name, func, region, mode, data } = props
    const obj = {};
    let allOption
    if (data !== "") {
        if (name === "region") {
            data.map((country) => obj[country[name]] = 0);
            allOption = Object.keys(obj).map((region) => {
                return (
                    <option value={region} key={region}>{region}</option>
                )
            })
        }

        if (name === "subregion") {
            data.map((country) => {
                if (region === "") {
                    obj[country.subregion] = 0
                }
                else {
                    if (country.region === region) {
                        obj[country.subregion] = 0
                    }
                }
            })
            allOption = Object.keys(obj).map((subRegion, index) => {
                return (
                    <option key={subRegion} value={subRegion} >{subRegion}</option>
                )
            })
        }

    }
    return (
        <div className="option">
            <select onClick={func} className={`selectOption ${mode ? "dark" : "light"}`} name="" id="">
                <option defaultValue value="">{defaultName}</option>
                {allOption}
            </select>
        </div>
    )
}