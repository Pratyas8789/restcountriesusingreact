import React from 'react'
import './Card.css'
import { Link } from "react-router-dom"

export default function Card(props) {
    const { filteredData, mode } = props
    const CountryCard = filteredData.map((country) => {

        return (
            <Link to={`${country.capital}`} >
                <div key={country.name.common} className="card">
                    <div className="flag">
                        <img src={country.flags.png} alt="" />
                    </div>
                    <div className="CountryName">
                        <h3 >{country.name.common}</h3>
                    </div>
                    <div className="CountryDetails">
                        <h3>Population:{country.population}</h3>
                        <h3>Region:{country.region}</h3>
                        <h3>Sub-region:{country.subregion}</h3>
                        <h3>Capital: {country.capital}</h3>
                        <h3>Area: {country.area}</h3>
                    </div>
                </div>
            </Link>
        )
    })
    return (
        <>
            {CountryCard.length > 0 ? CountryCard : <h1>Sorry, no result found !!</h1>}
        </>

    )
}
