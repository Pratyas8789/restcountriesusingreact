import React from 'react'
import './CountriesCards.css'
import Card from '../Card/Card';

export default function CountriesCards(props) {
    const { countryName, region, subRegion, sortby, mode, data, targetCountry } = props;

    let filteredData = data
    if (data !== "") {
        if (countryName !== "") {
            filteredData = filteredData.filter(country => ((country.name.common).toLowerCase()).includes(countryName));
        }
        if (region !== "") {
            filteredData = filteredData.filter(country => country.region === region);
        }

        if (subRegion !== "") {
            filteredData = filteredData.filter(country => country.subregion === subRegion);
        }

        function comparator(a, b) {
            if (sortby === "name") {
                if (a.name.common < b.name.common) {
                    return -1;
                } if (a.name.common > b.name.common) {
                    return 1;
                }
                return 0
            }
            if (sortby === "area") {
                if (a.area < b.area) {
                    return -1;
                } if (a.area > b.area) {
                    return 1;
                }
                return 0
            }
            if (sortby === "population") {
                if (a.population < b.population) {
                    return -1;
                } if (a.population > b.population) {
                    return 1;
                }
                return 0
            }
        }
        filteredData.sort(comparator)
    }

    return (
        <div className='CountriesCards'>
            {data !== "" ? <Card targetCountry={targetCountry} mode={mode} filteredData={filteredData} /> : ""}

        </div>
    )
}