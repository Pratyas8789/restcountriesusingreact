import React from 'react'
import './SearchBar.css'
import OptionForFilter from '../OptionForFilter/OptionForFilter'
export default function SearchBar(props) {
  const { data, handleSearchCountry, handleSelectForRegion, handleSelectForSubRegion, handleSort, mode, region } = props

  return (
    <div className='searchBar-container' >
      <div className={`search ${mode ? "dark" : "light"}`}>
        <i className="fa fa-search fa-2x" aria-hidden="true"></i>
        <input className={mode ? "dark" : "light"} type="search" name="" id="search" onChange={handleSearchCountry} placeholder='Search for a country...' />
      </div>
      <OptionForFilter data={data} defaultName={"Filter by Region"} name={"region"} func={handleSelectForRegion} mode={mode} />
      <OptionForFilter data={data} defaultName={"Filter by SubRegion"} name={"subregion"} region={region} func={handleSelectForSubRegion} mode={mode} />
      <div className="option">
        <select className={`selectOption ${mode ? "dark" : "light"}`} name="" id="optionForSort" onClick={handleSort} >
          <option defaultValue value="">Sort-by</option>
          <option value="name">--name--</option>
          <option value="area">--area--</option>
          <option value="population">--population--</option>
        </select>
      </div>
    </div>
  )
}