import React from 'react'
import './NavBar.css'
export default function NavBar(props) {
  const { changeTheme, mode } = props

  return (
    <div className={`navBar-container ${mode ? "dark" : "light"}`}>
      <h2>Where in the world? </h2>
      <h3 onClick={changeTheme} className="changeTheme" > <i className="fa fa-moon-o" aria-hidden="true"></i>Dark Mode</h3>
    </div>
  )
}
