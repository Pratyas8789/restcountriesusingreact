import React from 'react'

export default function FindCountryLanguage(props) {
  const { languageData } = props
  const allLanguage = []
  for (let key in languageData) {
    allLanguage.push(languageData[key] + " ")
  }
  return (
    <div>
      {allLanguage}
    </div>
  )
}
