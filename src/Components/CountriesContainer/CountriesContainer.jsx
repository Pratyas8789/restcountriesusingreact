import React from 'react'
import './CountriesContainer.css'
import SearchBar from '../SearchBar/SearchBar';
import CountriesCards from '../CountriesCards/CountriesCards';

export default function FilterableCountryCard(props) {
    const { mode, data, targetCountry } = props;

    const [countryName, setCountryName] = React.useState("")
    const [region, setRegion] = React.useState("")
    const [subRegion, setSubRegion] = React.useState("")
    const [sortby, setSortby] = React.useState("")


    const handleSearchCountry = (event) => {
        let countryName = event.target.value;
        countryName = countryName.toLowerCase();
        setCountryName(countryName);
    }

    const handleSelectForRegion = (event) => {
        setRegion(event.target.value);
    }

    const handleSelectForSubRegion = (event) => {
        setSubRegion(event.target.value);
    }

    const handleSort = (event) => {
        setSortby(event.target.value)
    }

    return (
        <div className={`CountriesContainer-container ${mode ? "dark" : "light"}`} >
            <SearchBar data={data} handleSearchCountry={handleSearchCountry} handleSelectForRegion={handleSelectForRegion} handleSelectForSubRegion={handleSelectForSubRegion} handleSort={handleSort} mode={mode} region={region} />
            <CountriesCards targetCountry={targetCountry} data={data} countryName={countryName} region={region} subRegion={subRegion} sortby={sortby} mode={mode} />
        </div>
    )
}
